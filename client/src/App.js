import React, { useState } from 'react';
import './App.css';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Axios from 'axios';
import LinearProgress from '@material-ui/core/LinearProgress';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import pozadina from '../src/bcgrnd.jpg';



function App() {

  const naslov = {
    fontSize: '45px',
    color: '#5c00e6',
    paddingTop: '3vh',
    textAlign: 'center',
    textTransform: 'uppercase',
    margin: 0
  }

  const paragraf = {
    fontSize: '26px',
    color: '#5c00e6',
    textAlign: 'center',
  }

  const tipOtplate = {
    paddingLeft: '10px',
    fontSize: '20pt',
    color: '#5c00e6',
    fontFamily: 'Big Shoulders Display',
  }

  const radioBtn = {
    paddingLeft: '10px',
    fontFamily: 'Big Shoulders Display',
  }

  const tableCell = {
    fontFamily: 'Big Shoulders Display',
    fontSize: '15pt',
    fontWeight: 'bold',
    borderTopWidth: 0.5,
    borderColor: "#5c00e6",
    borderStyle: 'solid',
    borderBottomWidth: 2,
    backgroundImage: `url(${pozadina})`,
  }

  const textF1 = {
    width: '13vw',
    padding: '5px',
    margin: 'auto',
  }

  const textF2 = {
    width: '10vw',
    padding: '5px',
    margin: 'auto',
  }

  const textF3 = {
    width: '15vw',
    padding: '5px',
    margin: 'auto',
  }

  const tabela = {
    display: 'block',
    margin: 'auto',
    width: '70vw',
  }

  const forma = {
    paddingTop: '5px',
    margin: 'auto',
    width: '70vw',
  }

  const dugme = {
    width: '14vw',
    height: '8vh',
    fontFamily: 'Big Shoulders Display',
    fontSize: '20pt'
  }

  const sve = {
    width: '100%',
    fontFamily: 'Big Shoulders Display',
    backgroundImage: `url(${pozadina})`,
    backgroundRepeat: 'repeat',
    backgroundSize: 'auto'
  }

  const radio = {
    width: '16vw',
    fontFamily: 'Big Shoulders Display',
  }

  const progres = {
    width: '70vw',
    margin: 'auto',
    marginBottom: '5px',
  }

  const nesto = {
    height: '100vh'
  }

  const StyledTableCell = withStyles((theme) => ({
    head: {
      fontFamily: 'Big Shoulders Display',
      fontSize: '20pt',
      fontWeight: 'bold',
      backgroundColor: '#5c00e6',
      color: theme.palette.common.white,
      borderTopWidth: 1, borderColor: "#5c00e6", borderStyle: 'solid',

    },
    body: {
    },
  }))(TableCell);

  const [iznos, setIznos] = useState();
  const [kamata, setKamata] = useState();
  const [period, setPeriod] = useState();
  const [rata, setRata] = useState();
  const [rate, setRate] = useState([]);
  const [columns, setColumns] = useState([]);
  const [stat, setStat] = useState(true);
  const [statPoruka, setStatPoruka] = useState(true);

  let clock = setInterval(() => {
    clearInterval(clock)
    clock = null
    if (stat)
      document.getElementById('lp').style.display = 'none'
    else
      document.getElementById('lp').style.display = 'block'

    if (statPoruka)
      document.getElementById('poruka').style.display = 'none'
    else
      document.getElementById('poruka').style.display = 'block'

    if (rata === undefined) {
      document.getElementById('anuitetnaRata').style.display = 'none'
    }
  }, 200)

  const izracunaj = () => {
    if (iznos === undefined || period === undefined || kamata === undefined) {
      setStatPoruka(false)
      document.getElementById('tabela').style.display = 'none'
    }
    else if (iznos.length === 0 || period.length === 0 || kamata.length === 0) {
      setStatPoruka(false)
      document.getElementById('tabela').style.display = 'none'
    }
    else {
      document.getElementById('tabela').style.display = 'block'
      setStatPoruka(true)

      Axios.post(`http://localhost:3001/api/delete`);

      if (izbor === 'a') {

        let kolone = [{ label: 'Iznos rate', align: 'center' },];
        setColumns(kolone);
        const rata = (iznos * (1 + kamata / 100)) / (period);
        setRata(rata.toFixed(2));
        setRate([]);

        document.getElementById('anuitetnaRata').style.display = 'block'
      }

      else {
        setStat(false);
        setRata('');

        let redniBroj = 0;
        let rataBezKamate = iznos / period;
        let mjesecnaKamata = kamata / 12;
        let noviIznos = iznos;
        let rate = [];

        for (let i = 0; i < period; i++) {
          noviIznos = noviIznos - rataBezKamate;
          let iznosMjesecneKamate = noviIznos / 100 * mjesecnaKamata;
          let mjesecnaRata = rataBezKamate + iznosMjesecneKamate;
          redniBroj++;
          rate.push([redniBroj, mjesecnaRata, noviIznos]);
        }

        let kolone = [
          { label: 'Redni broj', align: 'center' },
          { label: 'Iznos rate', align: 'center' },
          { label: 'Preostalo za plaćanje', align: 'center' },
        ];
        setColumns(kolone);

        Axios.post('http://localhost:3001/api/set', rate);

        document.getElementById('anuitetnaRata').style.display = 'none'

        Axios.get('http://localhost:3001/api/get').then((response) => {
          setRate(response.data);
          rate.sort((a, b) => a.redniBroj - b.redniBroj);
        }).then(setStat(true));
      }
    };
  };

  const [izbor, setIzbor] = useState('f');
  const handleChange = (event) => {
    setIzbor(event.target.value);
  };

  return (

    <div style={sve}>

      <h1 style={naslov}>Kreditni kalkulator</h1>
      <p style={paragraf}>Izračunajte ratu svog kredita</p>

      <div style={forma}>

        <TextField style={textF1}
          required
          id="outlined-required"
          label="Iznos kredita"
          variant="outlined"
          onChange={(e) => setIznos(e.target.value)}
        />

        <TextField style={textF2}
          required
          id="outlined-required"
          label="Kamata"
          variant="outlined"
          onChange={(e) => setKamata(e.target.value)}
        />

        <TextField style={textF3}
          required
          id="outlined-required"
          label="Period otplate(u mjesecima)"
          variant="outlined"
          onChange={(e) => setPeriod(e.target.value)}
        />

        <FormControl style={radio} component="fieldset">
          <FormLabel style={tipOtplate} component="legend">Tip otplate:</FormLabel>
          <RadioGroup row aria-label="gender" name="gender1" value={izbor} onChange={handleChange}>
            <FormControlLabel style={radioBtn} value="f" control={<Radio color="primary" />} label="Fiksna" />
            <FormControlLabel style={radioBtn} value="a" control={<Radio color="primary" />} label="Anuitetna" />
          </RadioGroup>
        </FormControl>

        <Button style={dugme}
          variant="contained"
          color="primary"
          size="large"
          onClick={() => { izracunaj(); }}>
          Izračunaj
            </Button>
      </div>

      <div style={progres} id="lp">
        <LinearProgress id="lp" />
      </div>

      <div id="tabela">
        <TableContainer style={tabela} component={Paper}>
          <Table aria-label="simple table" >

            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <StyledTableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}>
                    {column.label}
                  </StyledTableCell>
                ))}
              </TableRow>
            </TableHead>

            <TableBody>
              {rate.map((row) => (
                <TableRow key={row.redniBroj} >
                  <TableCell style={tableCell} align="center" component="th" scope="row">
                    {row.redniBroj}
                  </TableCell>
                  <TableCell style={tableCell} align="center">{row.iznosRate + " KM"}</TableCell>
                  <TableCell style={tableCell} align="center">{row.preostaloZaPlacanje + " KM"}</TableCell>
                </TableRow>
              ))}
          

            <TableRow>
              <TableCell style={tableCell} align="center" id="anuitetnaRata">{rata + " KM"}</TableCell>
            </TableRow>
            </TableBody>
          </Table>
        </TableContainer>

      </div>
      <div style={nesto} id="nesto">
        <p style={paragraf} id="poruka">Niste popunili sva polja!</p>
      </div>
    </div>
  )
}

export default App;
