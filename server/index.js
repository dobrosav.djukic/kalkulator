const bodyParser = require('body-parser');
const express = require('express');
const app = express();
const mysql = require('mysql');
const cors = require('cors');


const db = mysql.createPool({

    host: 'localhost',
    user: 'root',
    password: '',
    database: 'kalkulator',

});


app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }))



app.post("/api/delete", (req, res)=>{

    const sqlDelete ="TRUNCATE TABLE rate";
    db.query(sqlDelete, (err, result)=>{

        res.send(result);
    });
});

app.post("/api/set", (req, res) => {

    const rate = req.body;

    const sqlInsert = "INSERT INTO rate (redniBroj, iznosRate, preostaloZaPlacanje) VALUES ?"
    db.query(sqlInsert, [rate], (err, result) => {
        if (err) {
            return res.status(500).json(
                {
                    message: err
                }
            )
        }
        res.status(200).json({
            data: result
        })
    });

});

app.get("/api/get", (req, res)=>{

    const sqlGet = "SELECT * FROM rate";
    db.query(sqlGet, (err, result)=>{

        res.send(result);
    })
})


app.listen(3001, () => {
    console.log('running');
});